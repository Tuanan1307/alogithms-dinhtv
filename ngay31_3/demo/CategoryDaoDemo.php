<?php
require_once('./../dao/CategoryDAO.php');
require_once('./../entity/Category.php');

$database = new CategoryDao();

for($i=1; $i<=10 ; $i++)
{
    $category = new Category($i, 'Category '.$i);
    $database->insert($category);

}

$categoryInert = new Category(11, 'Category 12');
$categoryUpdate = new Category(6, 'Category new');

$database->insert($categoryInert);
$database->update($categoryUpdate);

echo '<pre>';
print_r($database->findAll('categoryTable'));

