<?php
require_once('./../dao/AccessoryDAO.php');
require_once('./../entity/Accessory.php');


$database = new AccessoryDao();

for($i=1; $i<=10 ; $i++)
{
    $accessory = new Accessory($i, 'Accessory '.$i);
    $database->insert($accessory);

}

$accessoryInsert = new Accessory(12, 'Accessory 12');
$accessoryUpdate = new Accessory(8, 'Accessory new');
$accessoryDelete = new Accessory(4, 'Accessory');

$database->insert($accessoryInsert);
$database->update($accessoryUpdate);
$database->delete($accessoryDelete);

echo '<pre>';
print_r($database->findAll('accessoryTable'));
