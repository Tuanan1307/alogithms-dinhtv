// Bài 9: Viết 1 class product theo thiết kế
class product {
    constructor(id, name, categoryId, saleDate, qulity, isDelete) {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.saleDate = saleDate;
        this.qulity = qulity;
        this.isDelete = isDelete;
    }
}

// Bài 10: Viết function tạo listProduct chứa 10 đối đượng product
function listMain(){
    const pushlistproduct = [];
    let product1 = new product(1, 'Product 1', 1, new Date(2022, 4, 13, 10, 40, 30), 0, true);
    pushlistproduct.push(product1);
    let product2 = new product(2, 'Product 2', 1, new Date(2022, 4, 13, 10, 40, 30), 2, true);
    pushlistproduct.push(product2);
    let product3 = new product(3, 'Product 3', 1, new Date(2022, 4, 13, 10, 40, 30), 0, true);
    pushlistproduct.push(product3);
    let product4 = new product(4, 'Product 4', 3, new Date(2022, 4, 13, 10, 40, 30), 1, false);
    pushlistproduct.push(product4);
    let product5 = new product(5, 'Product 5', 1, new Date(2022, 4, 13, 10, 40, 30), 2, false);
    pushlistproduct.push(product5);
    let product6 = new product(6, 'Product 6', 2, new Date(2022, 4, 13, 10, 40, 30), 0, true);
    pushlistproduct.push(product6);
    let product7 = new product(7, 'Product 7', 1, new Date(2022, 2, 13, 10, 40, 30), 3, false);
    pushlistproduct.push(product7);
    let product8 = new product(8, 'Product 8', 3, new Date(2022, 3, 13, 10, 40, 30), 1, true);
    pushlistproduct.push(product8);
    let product9 = new product(9, 'Pro  duct 9', 1, new Date(2022, 4, 13, 10, 40, 30), 0, true);
    pushlistproduct.push(product9);
    let product10 = new product(10, 'Product 10', 1, new Date(2022, 4, 13, 10, 40, 30), 0, true);
    pushlistproduct.push(product10);
    return pushlistproduct;
}
const listProduct = listMain();
console.log(listProduct);


// Bài 11:  Viết function trả về tên của product theo id: fiterProductById(listProduct, idProduct).
// Cách 1: Dùng vòng For
function fiterProductByIdWithFor(listProduct, idProduct){
    for (let product of listProduct){
        if (product.id === idProduct) {
            return product;
        }
    }
    return false;
}
console.log(fiterProductByIdWithFor(listProduct,1));

// Cách 2: Dùng ES6
function fiterProductById(listProduct, idProduct){
    const productName = listProduct.find(product => product.id === idProduct);
       return productName;
}
console.log(fiterProductById(listProduct,8));


// Bài 12: Viết function trả về array product có quality > 0 và chưa bị xóa: fiterProductByQulity(listProduct).
// Cách 1: Dùng vòng For
function fiterProductByQulityWithFor(listProduct) {
    const arrProduct = [];
    for (let product of listProduct) {
        if (product.qulity > 0 && product.isDelete === false) {
            arrProduct.push(product);
        }
    }
    return arrProduct;
}
console.log(fiterProductByQulityWithFor(listProduct));

// Cách 2: Dùng ES6
function fiterProductByQulity(listProduct){
    const arrProduct = listProduct.filter(product => product.qulity > 0 && product.isDelete === false);
        return arrProduct;
}
console.log(fiterProductByQulity(listProduct));


// Bài 13: Viết function trả về array tên product có saleDate > ngày hiện tại và chưa bị xóa: 
// Cách 1: Dùng vòng lặp For
function fiterProductBySaleDateWithFor(listProduct){
    var dateNow = new Date();
    const arrProduct = [];
    for (const product of listProduct) {
        if (product.saleDate - dateNow.getDate() > 0 && product.isDelete === false) {
            arrProduct.push(product);
        }
    }
    return arrProduct;
}
console.log(fiterProductBySaleDateWithFor(listProduct));

// Cách 2: Dùng ES6
function fiterProductBySaleDate(listProduct) {
    const arrProduct = listProduct.filter(product => product.saleDate - Date.now() > 0 && product.isDelete === false);
        return arrProduct;
}
console.log(fiterProductBySaleDate(listProduct));


// bài 14: Viết function trả về tổng số product ( tổng qulity) chưa bị xóa
// Cách 1: Không dùng reduce
function totalProductNoReduce(listProduct){
    var totalProduct = 0;
    const listFilterProduct = listProduct.filter(product => product.qulity > 0 && product.isDelete === false);
    for (product of listFilterProduct){
        totalProduct += product.qulity;
    }
    return totalProduct;
}
console.log(totalProductNoReduce(listProduct));

// Cách 2: Dùng reduce
function totalProduct(listProduct){
    const listFilterProduct = listProduct.filter(product => product.qulity > 0 && product.isDelete === false);
    const sumProduct = listFilterProduct.reduce((total, listFilterProduct) => total += listFilterProduct.qulity , 0);
    return sumProduct;
}
console.log(totalProduct(listProduct));

// Bài 15: Viết function trả về true nếu có product thuộc category:
// Cách 1: Dùng vòng lặp for
function isHaveProductInCategoryWithFor(listProduct, categoryId){
    for (const product of listProduct) {
        if (product.categoryId === categoryId) {
            return true;
        }
    }
    return false;
}
const checkCategoryIdWithFor = isHaveProductInCategoryWithFor(listProduct, 4);
if (checkCategoryIdWithFor === true) {
    console.log('Có giá trị');
} else {
    console.log('Không có giá trị nào');
}

// Cách 2: Dùng ES6
function isHaveProductInCategory(listProduct, categoryId){
    const categoryid = listProduct.find(product => product.categoryId === categoryId);
    if (categoryid) {
        return true;
    }
    return false;
}
const checkCategoryId = isHaveProductInCategory(listProduct, 2);
if (checkCategoryId === true) {
    console.log('Có giá trị');
} else {
    console.log('Không có giá trị nào');
}

// Bài 16: Viết function trả về array chứa array string (id, tên) product 
// có saleDate > ngày hiện tại và quality > 0: fiterProductBySaleDate(listProduct)
// Cách 1: Dùng vòng For
function filterProductBySaleDateWithFor(listProduct) {
    var dateNow = new Date();
    const arrProduct = [];
    for (const product of listProduct) {
        if (product.saleDate - dateNow.getDate() > 0 && product.qulity > 0) {
            arrProduct.push(product.id, product.name);
        }
    }
    return arrProduct;
}
console.log(filterProductBySaleDateWithFor(listProduct));

// Cách 2: Dùng ES6
function filterProductBySaleDate(listProduct) {
    var dateNow = new Date();
    const fiterProductBySaleDate = listProduct.filter(product => product.saleDate - dateNow.getDate() > 0 && product.qulity > 0);
    const listreduceProduct = fiterProductBySaleDate.map( (item) => {return {id: item.id, name: item.name};} );
    return listreduceProduct;
}
console.log(filterProductBySaleDate(listProduct));
