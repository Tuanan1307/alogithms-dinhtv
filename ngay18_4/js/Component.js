Vue.component ('button-counter', {
    // data phải là một hàm
    data: function() {
        return {
            count: 0,
        }
    },
    template: '<button v-on:click="count += 1">Bạn đã bấm {{ count }} lần.</button>',
});
new Vue({ el: '#components-demo' });
new Vue({ el: '#components-demo2' });


Vue.component('blog-post', {
    props: ['title'],
    template: '<h3>{{ title }}</h3>'
});
new Vue({ el: '#blog-post-demo' });


Vue.component('blog-post', {
  props: ['post'],
  template: '\
    <div class="blog-post">\
      <h3>{{ post.title }}</h3>\
      <button v-on:click="$emit(\'enlarge-text\', 0.1)">\
        Phóng to\
      </button>\
      <div v-html="post.content"></div>\
    </div>\
  '
})
new Vue({
  el: '#blog-posts-events-demo',
  data: {
    postFontSize: 1,
    posts: [
      { id: 1, title: 'Giới thiệu về Vue', content: '...nội dung...' },
      { id: 2, title: 'Các khái niệm trong Vue', content: '...nội dung...' },
      { id: 3, title: 'Vue cơ bản và vô cùng nâng cao', content: '...nội dung...' }
    ],
  }
});

Vue.component('alert-box', {
    template: `\
        <div class="demo-alert-box">\
          <strong>Lỗi!</strong>\
          <slot></slot>\
        </div>\
      `
})
new Vue({ el:'#slots-demo'});


