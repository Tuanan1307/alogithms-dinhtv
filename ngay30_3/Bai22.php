<?php
//Theo đệ quy
function calMonth($money, $rate,$month) {
    if ($month <= 0) {
        return $money;
    }
    else calSalary($money, $rate, $month) * 1.1;
    return calMonth($money, $rate,$month);
}
echo calMonth(1000,5);

//Không theo đệ quy
function calMonth1($money, $rate){
    $i = 0;
    do{
        $a = $money + $money * ($rate/100);
        $money = $a;
        $i++;
    }while($a < $money * 2);
    return $i;
}
echo '<br>';
echo calMonth1(1000,100);
