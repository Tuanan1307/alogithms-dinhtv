var obj = {
    foo: 'bar'
}
  
Object.freeze(obj)
  
new Vue({
    el: '#app',
    data: obj
})

// Vòng đời của một đối tượng
// hook "created" có thể được dùng để thực thi code sau khi một đối tượng được khởi tạo
    new Vue({
        data: {
            a: 1
        },
        created: function () {
            // `this` trỏ đến đối tượng Vue hiện hành
            console.log('giá trị của a là ' + this.a)
        }
    })
    // => "giá trị của a là 1"

// Các hook khác như mounted, updated, và destroyed cũng được gọi vào các giai đoạn khác nhau trong vòng đời của đối tượng.
// Tất cả các hook này đều được thực thi với ngữ cảnh this trỏ đến đối tượng Vue hiện hành
